#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 
#
#=================================================================================================
import maya.cmds as mc
import maya.OpenMaya as om
from functools import partial
from tileLib.tilePack import TilePolyQuadBlendInbetween

import shelf.youtube as yt

def play(*args):
    
    yt.play("google")

def QuadCompLocation(*args):
    if mc.objExists("Quad_Component_Location"):
        mc.viewFit( "Quad_Component_Location",f=0.1 )
    else:
        myCrv=mc.curve( n="Quad_Component_Location", d=1, p=[ (0, 0, 0) , (0, 1, 0) , (1, 1, 0) , (1, 1, 1) , (0, 1, 1) , (0, 0, 1) , (0, 0, 0) , (1, 0, 0) , (1, 0, 1) , (0, 0, 1) , (0, 0, 0) , (0, 1, 0) , (0, 1, 1) , (1, 1, 1) , (1, 1, 0) , (1, 0, 0) , (1, 0, 1) , (1, 1, 1) ] )
        myCrvShape=mc.listRelatives(myCrv,c=True)
        mc.setAttr( "%s.overrideEnabled"%(myCrvShape[0]), 1 )
        mc.setAttr( "%s.overrideColor"%(myCrvShape[0]), 14 )
        mc.viewFit( myCrv,f=0.1 )
        mc.setAttr( ("%s.overrideDisplayType"%(myCrvShape[0]) ), 2 )
        mc.select(d=True)

def myRun(*args):

    Surf1= mc.textField("tFld0", q=1 ,tx=1)
    Surf2= mc.textField("tFld1", q=1 ,tx=1)
    Comp1= mc.textField("tFld2", q=1 ,tx=1)
    Comp2= mc.textField("tFld3", q=1 ,tx=1)

    myN=TilePolyQuadBlendInbetween(Comp1,Comp2,Surf1,Surf2)
    #print myN
    mc.polySetToFaceNormal( myN[0] ,setUserNormal=True )

def addFirstSel(TextFName,*args):
    sel = mc.ls(sl=True)
    add = mc.textField(TextFName, edit=True, text=sel[0])
    

def sc_Tile_PolyQuad_Inbetween_Blend_UI():  
    ##UI##########################################################################################START
     
    #Window Titel 
      
    win=mc.window(title="Tile Poly Quad Blend Inbetween", iconName='Short Name', width=225 )

    col=mc.rowColumnLayout( numberOfColumns=2, columnAttach=(1, 'right', 0), columnWidth=[(1, 200), (2, 100)] )


    #Add Button Textfield

    but=mc.button(l='Add Base Mesh 1   (mesh)', c=partial(addFirstSel,"tFld0"))
    textFld0=mc.textField('tFld0')
    
    but=mc.button(l='Add Base Mesh 2   (mesh)', c=partial(addFirstSel,"tFld1"))
    textFld1=mc.textField('tFld1')

    but=mc.button(l='Add Component 1 (mesh)', c=partial(addFirstSel,"tFld2"))
    textFld2=mc.textField('tFld2')
    
    but=mc.button(l='Add Component 2 (mesh)', c=partial(addFirstSel,"tFld3"))
    textFld3=mc.textField('tFld3')


    #Autofill if 2 things are selected surf+comp
    quickSel=mc.ls(sl=True,fl=True)
    if len(quickSel)==4:
        mc.textField(textFld0, edit=True, text=quickSel[0])
        mc.textField(textFld1, edit=True, text=quickSel[1])
        mc.textField(textFld2, edit=True, text=quickSel[2])
        mc.textField(textFld3, edit=True, text=quickSel[3])

        
    #Run Button

    mc.text( label='' )
    mc.button( label='Run', command=myRun)

    #Run Button
    mc.text( label='Show Component Location' )
    mc.button( label='Loc', command=QuadCompLocation)
    
    #Help
    mc.text( label='' )
    mc.button( label='Help - Video', command=play )

    show=mc.showWindow()

    ##UI###########################################################################################END
