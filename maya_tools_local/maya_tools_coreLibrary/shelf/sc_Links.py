

def my_URLFinder(mykey):
    
    my_URLDict = { 
    'VTXColor' : "https://youtu.be/nxqr75-MMXI",
    'CutPlane' : "https://youtu.be/qDFl0sMJv7Y",
    'Contouring' : "https://youtu.be/6FRqnaA_GCI",
    'Creases' : "https://youtu.be/rfXQXsafvyw",
    '3dAssets' : "https://youtu.be/BFqzfHmH2Kk",
    'screenshot' : "https://youtu.be/l-KtiCKGHqE",
    'TessellateNurbs' : "https://youtu.be/5n2eKyjbqMs",
    'TileNurbsSingle' : "https://youtu.be/_KZvkQTRMWI",
    'TileNurbsBlend' : "https://youtu.be/2bwbYWkCYhg",
    'TileNurbsOffsetSingle' : "https://youtu.be/IVOH_YPURfU",
    'TileNurbsOffsetBlend' : "https://youtu.be/bVehiEwBuyU",
    'TileNurbsDiaSingle' : "https://youtu.be/apJCOCzs1r4",
    'TileNurbsDiaBlend' : "https://youtu.be/vx54erybwIM",
    'MapMeshToNurbs' : "https://youtu.be/MKeOW_v8zSc",
    'TessellateMesh' : "https://youtu.be/d1UwmoNnbes",
    'TilePolyQuadSingle' : "https://youtu.be/tRA2BYqHtes",
    'TilePolyQuadBlend' : "https://youtu.be/dEXQVWr9T8U",
    'TilePolyTriSingle' : "https://youtu.be/gDwSSHXjHF0",
    'TilePolyTriBlend' : "https://youtu.be/ot-A0eHv-Qw",
    'TilePolyHexSingle' : "https://youtu.be/YURiIAZFJSk",
    'TilePolyHexBlend' : "https://youtu.be/CPISkvflJEM",
    'VoxelMeshSingle' : "https://youtu.be/DADKHsnurxM",
    'VoxelMeshBlend' : "https://youtu.be/nPaKq3mXsWE",
    'ReorderMesh' : "https://youtu.be/qjTfbXFLL4g",
    'CreateMergeSet' : "https://youtu.be/qpTc7Wl7rnw",
    'NurbsVSMesh' : "https://youtu.be/xH23SA_rM2M"
    
    }
    return my_URLDict.get(mykey)