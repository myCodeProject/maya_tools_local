import maya.OpenMaya as om
import maya.cmds as mc
import math
from functools import partial
import tileLib.myAssetDictionary as AD
import shelf.youtube as yt

def play(*args):
    
    yt.play("3dAssets")

def QuadCompLocation(*args):
    if mc.objExists("Quad_Component_Location"):
        mc.viewFit( "Quad_Component_Location",f=0.1 )
    else:
        myCrv=mc.curve( n="Quad_Component_Location", d=1, p=[ (0, 0, 0) , (0, 1, 0) , (1, 1, 0) , (1, 1, 1) , (0, 1, 1) , (0, 0, 1) , (0, 0, 0) , (1, 0, 0) , (1, 0, 1) , (0, 0, 1) , (0, 0, 0) , (0, 1, 0) , (0, 1, 1) , (1, 1, 1) , (1, 1, 0) , (1, 0, 0) , (1, 0, 1) , (1, 1, 1) ] )
        myCrvShape=mc.listRelatives(myCrv,c=True)
        mc.setAttr( "%s.overrideEnabled"%(myCrvShape[0]), 1 )
        mc.setAttr( "%s.overrideColor"%(myCrvShape[0]), 14 )
        mc.viewFit( myCrv,f=0.1 )
        mc.setAttr( ("%s.overrideDisplayType"%(myCrvShape[0]) ), 2 )
        mc.select(d=True)

###create component
def myAsset_Comp_Gen(*args):
    
    assetType = mc.optionMenu( "AssetMenue",q=True,sl=True) #1 based list (optionmenue list starts at 1-n)
    #globals()["myQuad_Comp_%s"%(assetType)]("comp")
    
    name="Ex%s"%(assetType)

    try:
        V=AD.my_AssetFinder("%s_comp1"%(name))
        AD.geoStorageGen(V[1],V[2],V[3],V[4],V[5],V[6],V[7],V[0])
        
        V=AD.my_AssetFinder("%s_comp2"%(name))
        AD.geoStorageGen(V[1],V[2],V[3],V[4],V[5],V[6],V[7],V[0])
        
    except:
        pass


###create example 
def myAsset_Example_Gen(*args):
    
    assetType = mc.optionMenu( "AssetMenue",q=True,sl=True) #1 based list (optionmenue list starts at 1-n)
    #globals()["myQuad_Comp_%s"%(assetType)]("exmp")
    
    name="Ex%s"%(assetType)

    try:
        V=AD.my_AssetFinder(name)
        AD.geoStorageGen(V[1],V[2],V[3],V[4],V[5],V[6],V[7],V[0])
        
    except:
        pass


###change jpg
def printNewMenuItem( *args ):
        assetType = mc.optionMenu( "AssetMenue",q=True,sl=True) 
        path="//zaha-hadid.com/Data/Script_Library/Python/maya_tools_martin_pfleger/maya_tools_Shelf/icon_3dAsset/myQuad_Comp_%s.jpg"%(assetType)
        mc.picture("mypic", e=True , image=path)    
        

def sc_AssetGen_UI():          
    mc.window(title="Generate 3d Assets", iconName='Short Name', width=225 )

    #mc.columnLayout( adjustableColumn=True )

    mc.rowColumnLayout( numberOfColumns=2, columnAttach=(1, 'right', 0), columnWidth=[(1, 150), (2, 200)])

    #
    mc.text( label='3d Assets' )
    mc.optionMenu( "AssetMenue",label='', changeCommand=printNewMenuItem )
    mc.menuItem( label='1' ) #(optionmenue list starts at "1" not "0")
    mc.menuItem( label='2' )
    mc.menuItem( label='3' )
    mc.menuItem( label='4' )
    mc.menuItem( label='5' )
    mc.menuItem( label='6' )
    mc.menuItem( label='7' )
    mc.menuItem( label='8' )

    #
    mc.text( label='' )
    mc.text( label='' )
    mc.text( label='Component' )
    mc.button( label='Generate', command=myAsset_Comp_Gen )
    mc.text( label='Example' )
    mc.button( label='Generate', command=myAsset_Example_Gen )

    #Run Button
    mc.text( label='Show Component Location' )
    mc.button( label='Loc', command=QuadCompLocation)

    path="//zaha-hadid.com/Data/Script_Library/Python/maya_tools_martin_pfleger/maya_tools_Shelf/icon_3dAsset/myQuad_Comp_1.jpg"
    mc.text( label='Thumbnail' )
    pic=mc.picture("mypic", image=path )
    
    #Help
    mc.text( label='' )
    mc.button( label='Help - Video', command=play )

    mc.showWindow()






