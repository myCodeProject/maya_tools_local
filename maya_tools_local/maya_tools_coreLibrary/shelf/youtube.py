import os

def my_URLFinder(mykey,*args):
    
    my_URLDict = {
    'POLYMODELINGPOLYMODELINGPOLYMODELING' : "http://www.google.com",
    'google' : "http://www.google.com",
    'AlignTools' : "https://www.youtube.com/embed/7wMkbxVsU0s?rel=0",
    'MeasureTools' : "https://www.youtube.com/embed/LgqoFnWFtzs?rel=0",
    'PolyToNurbs' : "http://www.google.com",
    'VTXColor' : "https://www.youtube.com/embed/nxqr75-MMXI?rel=0",
    'ToCube' : "https://www.youtube.com/embed/md0KO3UFSQQ?rel=0",
    'Freeze' : "https://www.youtube.com/embed/JhBjKzpsvVs?rel=0",
    'CutPlane' : "https://www.youtube.com/embed/qDFl0sMJv7Y?rel=0",
    'Contouring' : "https://www.youtube.com/embed/6FRqnaA_GCI?rel=0",
    'Creases' : "https://www.youtube.com/embed/rfXQXsafvyw?rel=0",
    '3dAssets' : "https://www.youtube.com/embed/BFqzfHmH2Kk?rel=0",
    'screenshot' : "https://www.youtube.com/embed/l-KtiCKGHqE?rel=0",
    'NURBSTOOLSNURBSTOOLSNURBSTOOLS' : "http://www.google.com",
    'TesselateNurbs' : "https://www.youtube.com/embed/5n2eKyjbqMs?rel=0",
    'TileNurbsSingle' : "https://www.youtube.com/embed/_KZvkQTRMWI?rel=0",
    'TileNurbsBlend' : "https://www.youtube.com/embed/2bwbYWkCYhg?rel=0",
    'TileNurbsSingleInbetween' : "https://www.youtube.com/embed/AyC-TtNrGd0?rel=0",
    'TileNurbsBlendInbetween' : "https://www.youtube.com/embed/ElLJtmTKRSg?rel=0",
    'TileNurbsOffsetSingle' : "https://www.youtube.com/embed/IVOH_YPURfU?rel=0",
    'TileNurbsOffsetBlend' : "https://www.youtube.com/embed/bVehiEwBuyU?rel=0",
    'TileNurbsDiaSingle' : "https://www.youtube.com/embed/apJCOCzs1r4?rel=0",
    'TileNurbsDiaBlend' : "https://www.youtube.com/embed/vx54erybwIM?rel=0",
    'MapMeshToNurbs' : "https://www.youtube.com/embed/MKeOW_v8zSc?rel=0",
    'MESHTOOLSMESHTOOLSMESHTOOLS' : "http://www.google.com",
    'TesselateMesh' : "https://www.youtube.com/embed/d1UwmoNnbes?rel=0",
    'TilePolyQuadSingle' : "https://www.youtube.com/embed/tRA2BYqHtes?rel=0",
    'TilePolyQuadBlend' : "https://www.youtube.com/embed/dEXQVWr9T8U?rel=0",
    'TilePolyQuadSingleInbetween' : "https://www.youtube.com/embed/mV7YmgrniD8?rel=0",
    'TilePolyQuadBlendInbetween' : "https://www.youtube.com/embed/_X2WtlAZGwU?rel=0",
    'TilePolyQuadDistribute' : "https://www.youtube.com/embed/lgcGMR5jn3c?rel=0",
    'TilePolyTriSingle' : "https://www.youtube.com/embed/gDwSSHXjHF0?rel=0",
    'TilePolyTriBlend' : "https://www.youtube.com/embed/ot-A0eHv-Qw?rel=0",
    'TilePolyHexSingle' : "https://www.youtube.com/embed/YURiIAZFJSk?rel=0",
    'TilePolyHexBlend' : "https://www.youtube.com/embed/CPISkvflJEM?rel=0",
    'TilePolyNgonSingle' : "https://www.youtube.com/embed/zb1h1ZC5s1Q?rel=0",
    'TilePolyNgonBlend' : "https://www.youtube.com/embed/FxAk2Oo7Tfw?rel=0",
    'VoxelizeMeshSingle' : "https://www.youtube.com/embed/DADKHsnurxM?rel=0",
    'VoxelizeMeshBlend' : "https://www.youtube.com/embed/nPaKq3mXsWE?rel=0",
    'QuadMeshReorder' : "https://www.youtube.com/embed/qjTfbXFLL4g?rel=0",
    'CreateMergeSets' : "https://www.youtube.com/embed/qpTc7Wl7rnw?rel=0"
    
    }
    return my_URLDict.get(mykey)
    
    
def play(name,*args):

    link = my_URLFinder(name)
    try:
        os.startfile(link)
    except:
        pass