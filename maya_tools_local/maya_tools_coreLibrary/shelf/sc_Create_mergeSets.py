#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 120404
#
#=================================================================================================

###VERTEX SELECTION ONLY
import re
import maya.OpenMaya as om
import maya.cmds as mc
import math

def sc_Create_mergeSets_UI():

    sel=mc.ls(sl=True,fl=True)
    compShape=mc.listRelatives((sel[-1]),p=True)
    nr=mc.polyEvaluate(compShape,v=True)

    compShape2=mc.listRelatives((sel[0]),c=True)
    nr2=mc.polyEvaluate(compShape2[0],v=True)

    FaceTotal=nr2/nr

    selectionList=[]

    for i in range(1,len(sel)):
        x=re.split( '\[|\]' ,sel[i])
        
        for p in range(FaceTotal):
            if p==0:
                y=int(x[1])
            else:
                y=y+nr
                
            selectionList.append('%s.vtx[%s]'%(sel[0],y))
            

    mc.select(selectionList)

    mc.sets(n="myMergeSet_%s_01"%(sel[0]),text="gCharacterSet")





