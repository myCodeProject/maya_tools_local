#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 130621 
#
#=================================================================================================

#=================================================================================================
# Script: Reorder Mesh
# Dependencies : 
# Description : reorders quadMesh*
# How to run : select polygon edgeloop at boarder
#
#=================================================================================================

#====================================================================================
#Script18

import maya.cmds as mc
from reorderMeshLib.myQuadMeshReorder import myQuadMeshReorder

def sc_MeshReorder_UI():
    myQuadMeshReorder()

#====================================================================================
