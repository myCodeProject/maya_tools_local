#Align To Line

import maya.OpenMaya as om
import maya.cmds as mc
import math

def sc_Align_ToLine_UI():
    #match z of point
    myIsTrue=mc.selectPref(q=True, trackSelectionOrder=True)

    if myIsTrue==1:
        
        sel=mc.ls(os=True,fl=True)
        
        pointA=sel[0]
        pointB=sel[1]
        
        
        for i in range(2,len(sel)):		
            #normal of lineplane
            P1=mc.pointPosition(pointA)
            P2=mc.pointPosition(pointB)
            vP1=om.MVector(P1[0],P1[1],P1[2])
            vP2=om.MVector(P2[0],P2[1],P2[2])
            
            vP4=om.MVector(vP1.x,vP1.y,1)	
            AB2=vP2-vP1;
            AC2=vP4-vP1;
            N2=(AB2^AC2)
            N2.normalize()
            
            #normal of points plane
            a=mc.pointPosition(sel[i])
            A=om.MVector(a[0],a[1],a[2])
            B=om.MVector(A.x,A.y,(A.z+1))
            C=A+N2
            
            AB=B-A
            AC=C-A
            
            N=AB^AC
            N.normalize()
            
            P1=mc.pointPosition(pointA)
            P2=mc.pointPosition(pointB)
            vP1=om.MVector(P1[0],P1[1],P1[2])
            vP2=om.MVector(P2[0],P2[1],P2[2])
            vP3=A
            
            if(N*(vP2-vP1)!=0):
                print 'hi'
                u=(N*(vP3-vP1))/(N*(vP2-vP1))
                P=om.MVector()
                P=vP1+((vP2-vP1)*u)
                mc.move(P.x,P.y,P.z,sel[i],a=True)
                
    else:
        mc.error("Go Preferences > Selection --> Turn On TrackSelectionOrder")
