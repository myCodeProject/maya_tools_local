#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 
#
#=================================================================================================
import maya.cmds as mc
import maya.OpenMaya as om
from functools import partial
from tileLib.tilePack import TessellateNurbs
import shelf.youtube as yt

def play(*args):
    
    yt.play("TesselateNurbs")

def myRun(*args):
    
    selection=mc.ls(sl=True,fl=True)
    
    if len(selection) == 0:
        mc.warning("Nothing Selected; Select Nurbs Surface")
        
    if len(selection) != 0:
        #rebuild Nurbs parameter 0:1
        theShape=mc.listRelatives(selection[0])
        degreeU=mc.getAttr('%s.degreeU'%(theShape[0]))
        degreeV=mc.getAttr('%s.degreeV'%(theShape[0]))
        mc.rebuildSurface(selection[0],ch=False,rpo=1,rt=0,end=1,kr=0,kcp=1,kc=0,su=10,du=degreeU,sv=10,dv=degreeV,tol=0.0003,fr=0,dir=2)
        
        #get UI Values  
        Texture=mc.textField("my_Texture", q=1 ,tx=1)
        GridTyp = mc.optionMenu( "TesselMenu",q=True,v=True)
        u = mc.intField("my_U", q=1, v=1)
        v = mc.intField("my_V", q=1, v=1)
        
        #tesselate
        if Texture=="None" or Texture=="default" or Texture==0:
            name=TessellateNurbs(u,v,selection[0],GridTyp,"default")
        
        else:
            name=TessellateNurbs(u,v,selection[0],GridTyp,Texture)
        
        #cleanup result
        mc.polyMergeVertex(name[0],d=0.001 )
        mc.polyAverageNormal(name[0],prenormalize=True,allowZeroNormal=0,postnormalize=0,distance=0.1 ,replaceNormalXYZ=[1, 0, 0])
        mc.select(d=True)
        mc.select(name)

def printNewMenuItem( item ):
        print ("%s_Selected")%(item)
        
        if ("%s_Selected")%(item)=='regGrid_Selected':
            path="//zaha-hadid.com/Data/Script_Library/Python/maya_tools_martin_pfleger/maya_tools_Shelf/icon_Tessellation/regGrid.jpg"
            mc.picture("mypic", e=True , image=path)    
        
        if ("%s_Selected")%(item)=='diaGrid_Selected':
            path="//zaha-hadid.com/Data/Script_Library/Python/maya_tools_martin_pfleger/maya_tools_Shelf/icon_Tessellation/diaGrid.jpg"
            mc.picture("mypic", e=True , image=path)   
        
        if ("%s_Selected")%(item)=='triGrid_Selected':
            path="//zaha-hadid.com/Data/Script_Library/Python/maya_tools_martin_pfleger/maya_tools_Shelf/icon_Tessellation/triGrid.jpg"
            mc.picture("mypic", e=True , image=path)   
        
        if ("%s_Selected")%(item)=='skewGrid_Selected':
            path="//zaha-hadid.com/Data/Script_Library/Python/maya_tools_martin_pfleger/maya_tools_Shelf/icon_Tessellation/skewGrid.jpg"
            mc.picture("mypic", e=True , image=path)   
        
        if ("%s_Selected")%(item)=='regGridOffset_Selected':
            path="//zaha-hadid.com/Data/Script_Library/Python/maya_tools_martin_pfleger/maya_tools_Shelf/icon_Tessellation/regGridOffset.jpg"
            mc.picture("mypic", e=True , image=path)   
        
        if ("%s_Selected")%(item)=='hexGrid_Selected':
            path="//zaha-hadid.com/Data/Script_Library/Python/maya_tools_martin_pfleger/maya_tools_Shelf/icon_Tessellation/hexGrid.jpg"
            mc.picture("mypic", e=True , image=path)        
       

def sc_Tile_Nurbs_Tessellate_UI():
    ##UI##############################################################################################
     
    #Window Titel      
    mc.window(title="S1 Tesselate Nurbs", iconName='Short Name', width=225 )

    #Text Fields

    mc.rowColumnLayout( numberOfColumns=2, columnAttach=(1, 'right', 0), columnWidth=[(1, 200), (2, 100)] )
    mc.text( label='U' )
    myU = mc.intField("my_U",value=20)
    mc.text( label='V' )
    myV = mc.intField("my_V", value=20)

    mc.text( label='Optional: Texture Name' )
    myTexture = mc.textField("my_Texture", tx="")

    #Dropdown Menu

    mc.optionMenu( "TesselMenu",label='Tesselation', changeCommand=printNewMenuItem )
    mc.menuItem( label='regGrid' )
    mc.menuItem( label='diaGrid' )
    mc.menuItem( label='triGrid' )
    mc.menuItem( label='skewGrid' )
    mc.menuItem( label='regGridOffset' )
    mc.menuItem( label='hexGrid' )

    #Run Button
    mc.text( label='' )
    mc.text( label='' )
    mc.button( label='Run', command=myRun)
    
    path="//zaha-hadid.com/Data/Script_Library/Python/maya_tools_martin_pfleger/maya_tools_Shelf/icon_Tessellation/regGrid.jpg"
    mc.text( label='Thumbnail' )
    pic=mc.picture( "mypic", image=path ,h=100)
    
    #Help
    mc.text( label='' )
    mc.button( label='Help - Video', command=play )

    mc.showWindow()

    ##UI##############################################################################################
