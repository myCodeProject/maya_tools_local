import maya.cmds as mc
from contourLib.myCreateDynamicCutplane import myCreateDynamicCutplane

def sc_CutPlane_UI():

    ClippingPlaneName='myCutPlane'
    
    sel=mc.ls(sl=True,fl=True)
    myCreateDynamicCutplane(ClippingPlaneName,sel)