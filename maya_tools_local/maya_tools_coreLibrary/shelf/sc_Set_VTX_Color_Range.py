#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date :  
#
#=================================================================================================

#=================================================================================================
# Script:VTX Color Range
# Dependencies : 
# Description : 
# How to run : select mesh then run
#
#=================================================================================================

import maya.cmds as mc
import maya.OpenMaya as om
from geoLib.myBarycentricLib import faceToOffsetFace
from functools import partial
import shelf.youtube as yt

def play(*args):
    
    yt.play("VTXColor")

def vtxColorNewRange(myMinVal,myMaxVal,*args):
    
    NewMin = mc.floatField(myMinVal, q=1, v=1)
    NewMax = mc.floatField(myMaxVal, q=1, v=1)

    sel=mc.ls(sl=True,fl=True)
    
    x=faceToOffsetFace(sel[0],0)
    myVt=x.myFaceToOffsetFace()
    myCL=x.myColor() #color at face id
    
    OldMax = max(myCL, key=lambda item: item[0])
    OldMin = min(myCL, key=lambda item: item[0])
    
    OldRange = (OldMax[0] - OldMin[0])  
    NewRange = (NewMax - NewMin) 
    
    if OldRange!=0:
        for i in range(len(myCL)):
             
            NewValue = (((myCL[i][0] - OldMin[0]) * NewRange) / OldRange) + NewMin
            mc.polyColorPerVertex('%s.f[%s]'%(sel[0],i),rgb =(NewValue,NewValue,NewValue))
            #print NewValue
    else:
        print "Color Error"
        
       

def sc_Set_VTX_Color_Range_UI():       
    mc.window(title="vxt Color Range", iconName='Short Name', width=225 )

    #mc.columnLayout( adjustableColumn=True )

    mc.rowColumnLayout( numberOfColumns=2, columnAttach=(1, 'right', 0), columnWidth=[(1, 200), (2, 150)] )
    mc.text( label='min' )
    myMinVal = mc.floatField(value=0.0)
    mc.text( label='max' )
    myMaxVal = mc.floatField(value=1.0)

    mc.text( label='' )
    mc.button( label='Run', command=partial(vtxColorNewRange,myMinVal, myMaxVal))
    
    #Help
    mc.text( label='' )
    mc.button( label='Help - Video', command=play )

    mc.showWindow()


