import maya.cmds as mc


def sc_To_1x1x1_Cube_UI():  

    sel=mc.ls(sl=True,fl=True)

    name=mc.lattice (sel[0] , dv=[2,2,2], objectCentered=True, ldv=[2,2,2])

    mc.move(0,0,0, "%s.pt[0][0][0]"%(name[1]),a=True,xyz=True,)
    mc.move(1,0,0, "%s.pt[1][0][0]"%(name[1]),a=True,xyz=True,)
    mc.move(1,1,0, "%s.pt[1][1][0]"%(name[1]),a=True,xyz=True,)
    mc.move(0,1,0, "%s.pt[0][1][0]"%(name[1]),a=True,xyz=True,)


    mc.move(0,0,1, "%s.pt[0][0][1]"%(name[1]),a=True,xyz=True,)
    mc.move(1,0,1, "%s.pt[1][0][1]"%(name[1]),a=True,xyz=True,)
    mc.move(1,1,1, "%s.pt[1][1][1]"%(name[1]),a=True,xyz=True,)
    mc.move(0,1,1, "%s.pt[0][1][1]"%(name[1]),a=True,xyz=True,)


    mc.delete(sel[0],ch=True)




