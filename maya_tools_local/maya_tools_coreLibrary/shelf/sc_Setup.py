import maya.cmds as mc

def sc_Setup_UI(): 

    mc.select(deselect=True)

    isOn=mc.selectPref( q=True, trackSelectionOrder=0)


    if isOn==False:
        mc.selectPref( trackSelectionOrder=1)

    print isOn

    mc.inViewMessage( amg='<hl>ZHA Shelf</hl> Ready to use', pos='midCenter', fade=True )
