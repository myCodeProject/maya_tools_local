import maya.cmds as mc

def sc_Freeze_UI():
    mc.DeleteHistory()
    mc.FreezeTransformations()
    mc.CenterPivot()