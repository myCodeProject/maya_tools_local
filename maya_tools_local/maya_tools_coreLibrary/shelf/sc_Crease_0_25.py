import maya.cmds as mc

def sc_Crease_0_25_UI():  

    mySmoothValue=0.25

    sel=mc.ls(sl=True,fl=True)
    allEdg=mc.filterExpand( ex=True, sm=32 )
    allVtx=mc.filterExpand( ex=True, sm=31 )

    if allEdg!=None:
        mc.polyCrease(allEdg,ch=False, value=mySmoothValue, vertexValue=mySmoothValue)

    if allVtx!=None:
        mc.polyCrease(allVtx,ch=False, value=0, vertexValue=mySmoothValue)

