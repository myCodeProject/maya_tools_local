#Align To Line EQ+

import maya.OpenMaya as om
import maya.cmds as mc
import math

def sc_Align_ToLineEq_UI():

    #match z of point
    myIsTrue=mc.selectPref(q=True, trackSelectionOrder=True)

    if myIsTrue==1:
        
        sel=mc.ls(os=True,fl=True)
        
        pointA=sel[0]
        pointB=sel[1]
        
        
        div = len(sel)-1
        count=1
        
        for i in range(2,len(sel)):		
            #normal of lineplane
            P1=mc.pointPosition(pointA)
            P2=mc.pointPosition(pointB)
            vP1=om.MVector(P1[0],P1[1],P1[2])
            vP2=om.MVector(P2[0],P2[1],P2[2])
            
            
            print div
            print count
            
            AB2=((vP2-vP1)/div )*count;
            count+=1
            
            
            P=om.MVector()
            P=AB2+vP1
            mc.move(P.x,P.y,P.z,sel[i],a=True)
                
    else:
        mc.error("Go Preferences > Selection --> Turn On TrackSelectionOrder")
