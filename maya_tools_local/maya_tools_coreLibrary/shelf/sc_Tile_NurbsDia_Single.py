#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 
#
#=================================================================================================
import maya.cmds as mc
import maya.OpenMaya as om
from functools import partial
from tileLib.tilePack import TileNurbsSingleDiagrid
import shelf.youtube as yt

def play(*args):
    
    yt.play("TileNurbsDiaSingle")

def QuadCompLocation(*args):
    if mc.objExists("Quad_Component_Location"):
        mc.viewFit( "Quad_Component_Location",f=0.1 )
    else:
        myCrv=mc.curve( n="Quad_Component_Location", d=1, p=[ (0, 0, 0) , (0, 1, 0) , (1, 1, 0) , (1, 1, 1) , (0, 1, 1) , (0, 0, 1) , (0, 0, 0) , (1, 0, 0) , (1, 0, 1) , (0, 0, 1) , (0, 0, 0) , (0, 1, 0) , (0, 1, 1) , (1, 1, 1) , (1, 1, 0) , (1, 0, 0) , (1, 0, 1) , (1, 1, 1) ] )
        myCrvShape=mc.listRelatives(myCrv,c=True)
        mc.setAttr( "%s.overrideEnabled"%(myCrvShape[0]), 1 )
        mc.setAttr( "%s.overrideColor"%(myCrvShape[0]), 14 )
        mc.viewFit( myCrv,f=0.1 )
        mc.setAttr( ("%s.overrideDisplayType"%(myCrvShape[0]) ), 2 )
        mc.select(d=True)

def myRun(*args):
    u = mc.intField("my_U", q=1, v=1)
    v = mc.intField("my_V", q=1, v=1)
    Height = mc.floatField("my_H", q=1, v=1)
    Surf= mc.textField("tFld0", q=1 ,tx=1)
    Comp1= mc.textField("tFld1", q=1 ,tx=1)

    myN=TileNurbsSingleDiagrid(u,v,Height,'A',Surf,Comp1)
    mc.polySetToFaceNormal( myN[0] ,setUserNormal=True )


def addFirstSel(TextFName,*args):
    sel = mc.ls(sl=True)
    add = mc.textField(TextFName, edit=True, text=sel[0])

    
def sc_Tile_NurbsDia_Single_UI(): 
    ##UI##############################################################################################
     
    #Window Titel    
    win=mc.window(title="S25 Tile Nurbs Single Diagrid", iconName='Short Name', width=225 )

    #Text Fields

    col=mc.rowColumnLayout( numberOfColumns=2, columnAttach=(1, 'right', 0), columnWidth=[(1, 200), (2, 100)] )

    mc.text( label='U' )
    myU = mc.intField("my_U",value=20)
    mc.text( label='V' )
    myV = mc.intField("my_V",value=20)

    mc.text( label='Height' )
    myH = mc.floatField("my_H",value=1.0)


    #Add Button Textfield

    but=mc.button(l='Add Base Surface (nurbs)', c=partial(addFirstSel,"tFld0"))
    textFld0=mc.textField('tFld0')

    but=mc.button(l='Add Component   (mesh)', c=partial(addFirstSel,"tFld1"))
    textFld1=mc.textField('tFld1')


    #Autofill if 2 things selected surf+comp
    quickSel=mc.ls(sl=True,fl=True)
    if len(quickSel)==2:
        mc.textField(textFld0, edit=True, text=quickSel[0])
        mc.textField(textFld1, edit=True, text=quickSel[1])
        
        
    #Run Button
    mc.text( label='' )
    mc.button( label='Run', command=myRun)

    #Run Button
    mc.text( label='Show Component Location' )
    mc.button( label='Loc', command=QuadCompLocation)
    
    #Help
    mc.text( label='' )
    mc.button( label='Help - Video', command=play )

    show=mc.showWindow()

    ##UI##############################################################################################

