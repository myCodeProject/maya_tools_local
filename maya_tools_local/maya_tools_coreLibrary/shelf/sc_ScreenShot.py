###Screenshot From Maya

from maya.OpenMaya import MImage
import maya.cmds as mc
import os
import getpass
import shelf.youtube as yt

def play(*args):
    
    yt.play("screenshot")


def imageconvert(path,type="png"):
	# type = "jpg"
	parsepath = os.path.splitext(path)
	#print parsepath
	mayaImage = MImage()
	mayaImage.readFromFile(path)
	#print parsepath[0][:-5]
	mayaImage.writeToFile( "%s.%s" % (parsepath[0][:-5],type),type)

def playblast(path,filename,resolution=[4000,3000]):

	#mc.displayRGBColor("background",0.36,0.36,0.36)

	outpath = mc.playblast(frame=1,fmt="image",viewer=0,fp=4,orn=0,p=100,wh=resolution,ifz=0,fo=1,offScreen=1,f=path+filename)
	#print outpath
	path1 = os.path.splitext(outpath) 
	realpath = os.path.splitext(path1[0])[0]+".0000.iff"
	# print realpath
	
	return realpath

def ss(path='C:\\Users\\martin.p\\Desktop\\EXPORT\\',filename='screenshot',type='jpg',r=[200,200]):
	# print path,filename,type
	iffpath = playblast(path,filename,resolution=r)
	imageconvert(iffpath,type)
	os.remove(iffpath)

###

def runSS(*args):
    name = mc.textField("myN", q=1 ,tx=1)
    nH = mc.intField("my_H", q=1 ,v=1)
    nW = mc.intField("my_W", q=1 ,v=1)
    userName=getpass.getuser()
    myPath="C:\\Users\\%s\\Desktop\\MayaScreenshot\\"%(userName)
    ss(myPath,name,"jpg",[nW,nH])

    

def sc_ScreenShot_UI():
    ##UI##############################################################################################
     
    #Window Titel      
    mc.window(title="Maya Screen Shot", iconName='Short Name', width=225 )

    #Text Fields

    mc.rowColumnLayout( numberOfColumns=2, columnAttach=(1, 'right', 0), columnWidth=[(1, 120), (2, 300)] )
    mc.text( label='Resolution: H (pixel)' )
    myH = mc.intField("my_H",value=1000)
    mc.text( label='Resolution: W (pixel)' )
    myW = mc.intField("my_W",value=2000)

    mc.text( label='Name:' )
    myName = mc.textField("myN",tx="ScreenShot_01")

    userName=getpass.getuser()
    myPath="C:\\Users\\%s\\Desktop\\MayaScreenshot\\"%(userName)
    mc.text( label='Destination: ' )
    mc.text( label=myPath )

    #Run Button
    mc.text( label='' )
    mc.button( label='Take Screen Shot', command=runSS)
    
    #Help
    mc.text( label='' )
    mc.button( label='Help - Video', command=play )

    mc.showWindow()

    ##UI##############################################################################################
    
