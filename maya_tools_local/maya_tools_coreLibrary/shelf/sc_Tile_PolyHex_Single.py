#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 
#
#=================================================================================================
import maya.cmds as mc
import maya.OpenMaya as om
from functools import partial
from tileLib.tilePack import TilePolyHexSingle
import shelf.youtube as yt

def play(*args):
    
    yt.play("TilePolyHexSingle")
    
def HexCompLocation(*args):
    if mc.objExists("Hex_Component_Location"):
        mc.viewFit( "Hex_Component_Location",f=0.1 )
    else:
        myCrv=mc.curve( n="Hex_Component_Location", d=1, p=[ (-2, -1, 0 ),( -2, 1, 0 ),( -2, 1 ,1 ),( 0, 2 ,1 ),( 0, 2 ,0 ),( 2, 1 ,0 ),( 2, 1, 1 ),( 2, -1, 1 ),( 2, -1, 0 ),( 0, -2, 0 ),( 0, -2, 1 ),( -2, -1, 1 ),( -2, -1, 0 ),( -2, 1, 0 ),( 0, 2 ,0 ),( 0, 2, 1 ),( 2, 1 ,1 ),( 2 ,1 ,0 ),( 2 ,-1, 0 ),( 2, -1, 1 ),( 0, -2 ,1 ),( 0, -2 ,0 ),( -2, -1 ,0 ),( -2, -1, 1 ),( -2, 1, 1) ] )
        myCrvShape=mc.listRelatives(myCrv,c=True)
        mc.setAttr( "%s.overrideEnabled"%(myCrvShape[0]), 1 )
        mc.setAttr( "%s.overrideColor"%(myCrvShape[0]), 14 )
        mc.viewFit( myCrv,f=0.1 )
        mc.setAttr( ("%s.overrideDisplayType"%(myCrvShape[0]) ), 2 )
        mc.select(d=True)

def myRun(*args):

    Height = mc.floatField("my_H", q=1, v=1)
    Surf= mc.textField("tFld0", q=1 ,tx=1)
    Comp1= mc.textField("tFld1", q=1 ,tx=1)

    myN=TilePolyHexSingle(Comp1,Surf,Height)
    #print myN
    mc.polySetToFaceNormal( myN[0] ,setUserNormal=True )

def addFirstSel(TextFName,*args):
    sel = mc.ls(sl=True)
    add = mc.textField(TextFName, edit=True, text=sel[0])
    
def sc_Tile_PolyHex_Single_UI():     
    ##UI##########################################################################################START
     
    #Window Titel 
      
    win=mc.window(title="S20 Tile Poly Hex Single", iconName='Short Name', width=225 )

    col=mc.rowColumnLayout( numberOfColumns=2, columnAttach=(1, 'right', 0), columnWidth=[(1, 200), (2, 100)] )

    #Text Fields

    mc.text( label='Height' )
    myH = mc.floatField("my_H",value=1.0)

    #Add Button Textfield

    but=mc.button(l='Add Base Mesh   (mesh)', c=partial(addFirstSel,"tFld0"))
    textFld0=mc.textField('tFld0')

    but=mc.button(l='Add Component (mesh)', c=partial(addFirstSel,"tFld1"))
    textFld1=mc.textField('tFld1')


    #Autofill if 2 things are selected surf+comp
    quickSel=mc.ls(sl=True,fl=True)
    if len(quickSel)==2:
        mc.textField(textFld0, edit=True, text=quickSel[0])
        mc.textField(textFld1, edit=True, text=quickSel[1])

        
    #Run Button

    mc.text( label='' )
    mc.button( label='Run', command=myRun)

    #Loc
    mc.text( label='Show Component Location' )
    mc.button( label='Loc', command=HexCompLocation)
    
    #Help
    mc.text( label='' )
    mc.button( label='Help - Video', command=play )

    show=mc.showWindow()

    ##UI###########################################################################################END
