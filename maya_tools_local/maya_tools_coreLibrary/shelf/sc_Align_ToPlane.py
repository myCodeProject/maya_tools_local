#Align To Plane

import maya.OpenMaya as om
import maya.cmds as mc
import math

def sc_Align_ToPlane_UI():

    #match z of point
    myIsTrue=mc.selectPref(q=True, trackSelectionOrder=True)

    if myIsTrue==1:
        
        sel=mc.ls(os=True,fl=True)
        
        pointA=sel[0]
        pointB=sel[1]
        pointC=sel[2]
        
        for i in range(3,len(sel)):
            A=mc.pointPosition(pointA)
            B=mc.pointPosition(pointB)
            C=mc.pointPosition(pointC)
            
            vA=om.MVector(A[0],A[1],A[2])
            vB=om.MVector(B[0],B[1],B[2])
            vC=om.MVector(C[0],C[1],C[2])
            
            AB=vB-vA
            AC=vC-vA
            
            N=AB^AC
            N.normalize()
            
            P=mc.pointPosition(sel[i])
            vP1=om.MVector(P[0],P[1],P[2])
            vP2=vP1+N
            vP3=vA
            
            if(N*(vP2-vP1)!=0):
                u=(N*(vP3-vP1))/(N*(vP2-vP1))
                
                P=om.MVector()
                P=vP1+((vP2-vP1)*u)
                mc.move(P.x,P.y,P.z,sel[i],a=True)
                
    else:
        mc.error("Go Preferences > Selection --> Turn On TrackSelectionOrder")
