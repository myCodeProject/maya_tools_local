import maya.cmds as mc

name="in_mesh_out_area"

def myNodeLoad(name):
    '''
    Load glToolsTools plugin.
    '''
    # Check if plugin is loaded
    if not mc.pluginInfo(name,q=True,l=True):
         
        # Load Plugin
        try: mc.loadPlugin(name)
        except: raise Exception('Unable to load glToolsTools plugin!')
     
    # Return Result
    return 1
    
myNodeLoad(name)

def myMeshAreaConnection(meshName):

    ####################
    index=meshName
    meshName=meshName
    ####################
    
    meshNameShape=mc.listRelatives(meshName,c=True)
    
    myParticle='myParticle_%s'%(index)
    
    #create Particle
    myCenter=mc.objectCenter(meshName)
    pName=mc.particle(n=myParticle,p=(myCenter),c=1)
    pNameShape=mc.listRelatives(pName,c=True)
    
    #set up attributes on particle
    
    mc.setAttr(('%s.particleRenderType'%(pNameShape[0])),2)
    #
    mc.select(pNameShape)
    mc.addAttr(internalSet=True, ln="pointSize", at="long", min=1, max=60, dv=2)
    mc.addAttr(internalSet=True, ln="selectedOnly", at="bool", dv=False )
    mc.addAttr(internalSet=True, dt="string", ln="attributeName")
    
    #
    mc.addAttr(ln="myArea", at="double")
    mc.setAttr(('%s.myArea'%(pNameShape[0])),keyable=True)
    
    #
    mc.setAttr(('%s.attributeName'%(pNameShape[0])),"myArea", type="string" )
    
    #create my custom node
    myAreaNodeName=mc.createNode("myMeshAreaNode",n="meshArea_%s"%(index))
    
    #connect mesh to my custom node
    mc.connectAttr( ("%s.outMesh"%(meshNameShape[0])), ("%s.Input"%(myAreaNodeName)), force=True)
    
    #connect my custom node to particle render number
    mc.connectAttr( ("%s.Output"%(myAreaNodeName)), ("%s.myArea"%(pNameShape[0])), force=True)
    
    #
    mc.connectAttr( ("%s.translate"%(meshName)), ("%s.translate"%(pName[0])) ,f=True)
    
    #freez transform of mesh
    mc.makeIdentity(meshName,apply=True,t=1,jointOrient=True)
    
    
    
def sc_MeshArea_UI():

    sel=mc.ls(sl=True,fl=True)
    myMeshAreaConnection(sel[0])

