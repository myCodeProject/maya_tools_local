#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 
#
#=================================================================================================

import maya.cmds as mc
import maya.OpenMaya as om
from functools import partial
from tileLib.tilePack import TilePolyQuadDistribute
import shelf.youtube as yt

def play(*args):
    
    yt.play("TilePolyQuadDistribute")

def QuadCompLocation(*args):
    if mc.objExists("Quad_Component_Location"):
        mc.viewFit( "Quad_Component_Location",f=0.1 )
    else:
        myCrv=mc.curve( n="Quad_Component_Location", d=1, p=[ (0, 0, 0) , (0, 1, 0) , (1, 1, 0) , (1, 1, 1) , (0, 1, 1) , (0, 0, 1) , (0, 0, 0) , (1, 0, 0) , (1, 0, 1) , (0, 0, 1) , (0, 0, 0) , (0, 1, 0) , (0, 1, 1) , (1, 1, 1) , (1, 1, 0) , (1, 0, 0) , (1, 0, 1) , (1, 1, 1) ] )
        myCrvShape=mc.listRelatives(myCrv,c=True)
        mc.setAttr( "%s.overrideEnabled"%(myCrvShape[0]), 1 )
        mc.setAttr( "%s.overrideColor"%(myCrvShape[0]), 14 )
        mc.viewFit( myCrv,f=0.1 )
        mc.setAttr( ("%s.overrideDisplayType"%(myCrvShape[0]) ), 2 )
        mc.select(d=True)

def myRun(*args):

    Height = mc.floatField("my_H", q=1, v=1)

    Surf= mc.textField("tFld0", q=1 ,tx=1)
    Comp1= mc.textField("tFld1", q=1 ,tx=1)
    Comp2= mc.textField("tFld2", q=1 ,tx=1)
    Comp3= mc.textField("tFld3", q=1 ,tx=1)
    Comp4= mc.textField("tFld4", q=1 ,tx=1)
    Comp5= mc.textField("tFld5", q=1 ,tx=1)
    Comp6= mc.textField("tFld6", q=1 ,tx=1)
    Comp7= mc.textField("tFld7", q=1 ,tx=1)
    Comp8= mc.textField("tFld8", q=1 ,tx=1)
    
    compList=[Comp1,Comp2,Comp3,Comp4,Comp5,Comp6,Comp7,Comp8]
    compList2=[]
    
    for i in range(8):
        if compList[i]!="":
            compList2.append(compList[i])
    

    myN=TilePolyQuadDistribute(compList2,Surf,Height)
    #print myN
    mc.polySetToFaceNormal( myN[0] ,setUserNormal=True )

def addFirstSel(TextFName,*args):
    sel = mc.ls(sl=True)
    add = mc.textField(TextFName, edit=True, text=sel[0])

def sc_Tile_PolyQuad_Distribute_UI(): 
    ##UI##########################################################################################START
     
    #Window Titel 
      
    win=mc.window(title="S31 Tile Poly Quad Distribute", iconName='Short Name', width=225 )

    col=mc.rowColumnLayout( numberOfColumns=2, columnAttach=(1, 'right', 0), columnWidth=[(1, 200), (2, 100)] )

    #Text Fields

    mc.text( label='Height' )
    myH = mc.floatField("my_H",value=1.0)

    #Add Button Textfield

    but=mc.button(l='Add Base Mesh      (mesh)', c=partial(addFirstSel,"tFld0"))
    textFld0=mc.textField('tFld0')

    but=mc.button(l='Add Component 1 (mesh)', c=partial(addFirstSel,"tFld1"))
    textFld1=mc.textField('tFld1')

    but=mc.button(l='Add Component 2 (mesh)', c=partial(addFirstSel,"tFld2"))
    textFld2=mc.textField('tFld2')
    
    but=mc.button(l='Optional:   Add Component 3 (mesh)', c=partial(addFirstSel,"tFld3"))
    textFld2=mc.textField('tFld3')
    
    but=mc.button(l='Optional:   Add Component 4 (mesh)', c=partial(addFirstSel,"tFld4"))
    textFld2=mc.textField('tFld4')
    
    but=mc.button(l='Optional:   Add Component 5 (mesh)', c=partial(addFirstSel,"tFld5"))
    textFld2=mc.textField('tFld5')
    
    but=mc.button(l='Optional:   Add Component 6 (mesh)', c=partial(addFirstSel,"tFld6"))
    textFld2=mc.textField('tFld6')
    
    but=mc.button(l='Optional:   Add Component 7 (mesh)', c=partial(addFirstSel,"tFld7"))
    textFld2=mc.textField('tFld7')
    
    but=mc.button(l='Optional:   Add Component 8 (mesh)', c=partial(addFirstSel,"tFld8"))
    textFld2=mc.textField('tFld8')
    

    #Autofill if 3 things are selected surf+comp1+comp2

    #quickSel=mc.ls(sl=True,fl=True)
    #if len(quickSel)==3:
    #    mc.textField(textFld0, edit=True, text=quickSel[0])
    #    mc.textField(textFld1, edit=True, text=quickSel[1])
    #    mc.textField(textFld2, edit=True, text=quickSel[2])
        
    #Run Button

    mc.text( label='' )
    mc.button( label='Run', command=myRun)

    #Run Button
    mc.text( label='Show Component Location' )
    mc.button( label='Loc', command=QuadCompLocation)
    
    #Help
    mc.text( label='' )
    mc.button( label='Help - Video', command=play )

    show=mc.showWindow()

    ##UI###########################################################################################END
