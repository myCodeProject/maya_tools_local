# #########################
# polyToSubdiv2 V1.4 - 26/09/2014
#
# This script converts a polygon into a subd or NURBS surface
# it retains the creased edges and sharp corners.
#
# Please take into account that it will be rather
# slow when used on very complex geometry.
#
# written by Richard Wasenegger, 2014
# Richard.Wasenegger@zaha-hadid.com
# richard@wasenegger.at
# #########################


from maya.cmds import *
import math
import re
import maya.mel as mel
from geoLib.myStats import myStats


# ###############################################################
# Dependencies: vector.py
# ###############################################################

# Multiplies an vector with a scalar
def vmul(vector, scalar):
	return [vector[0]*scalar, vector[1]*scalar, vector[2]*scalar]
# end vmul()


# Gets the distance between two points
def dist(vector1, vector2):
	return math.sqrt(math.pow(vector2[0]-vector1[0], 2) + math.pow(vector2[1]-vector1[1], 2) + math.pow(vector2[2]-vector1[2], 2))
# end dist()

# Gets the world matrix of an maya object
def createMatrixFromObject(obj):
	m = getAttr(obj + ".worldMatrix")
	fScale = {"mm":10.0, "cm":1.0, "m":0.01, "km":0.00001, 
		"in":0.393701, "ft":0.0328084, "yd":0.0109361, "mi":6.2137e-6}[currentUnit(l=True, q=True)]
	return [
		[m[0], m[4], m[8], m[12]*fScale],
		[m[1], m[5], m[9], m[13]*fScale],
		[m[2], m[6], m[10], m[14]*fScale],
		[m[3], m[7], m[11], m[15]]]
# end createMatrixFromObject()


# Applies transformations to a point
def trp(matrix, point):
	p = point
	m = matrix
	
	w = m[3][0] * p[0] + m[3][1] * p[1] + m[3][2] * p[2] + m[3][3]
	
	return [
		(m[0][0] * p[0] + m[0][1] * p[1] + m[0][2] * p[2] + m[0][3]) / w,
		(m[1][0] * p[0] + m[1][1] * p[1] + m[1][2] * p[2] + m[1][3]) / w,
		(m[2][0] * p[0] + m[2][1] * p[1] + m[2][2] * p[2] + m[2][3]) / w]
# end trp()


# ###############################################################
# Dependencies: tools.py
# ###############################################################

global GLOBALS
GLOBALS = {}


# Gets the absolute position of a mesh cv
def getMeshCV(mesh, cv, absolute=True):
	# get inner offset
	vtx = mesh + ".vrts[" + str(cv) + "]"
	o = getAttr(vtx)[0]
	
	# Get units, correct vrts value
	fScale = {"mm":10.0, "cm":1.0, "m":0.01, "km":0.00001, 
		"in":0.393701, "ft":0.0328084, "yd":0.0109361, "mi":6.2137e-6}[currentUnit(l=True, q=True)]
	o = vmul(o, fScale)
	
	# get offset
	vtx = mesh + ".vtx[" + str(cv) + "]"
	pt = getAttr(vtx)[0]
	
	# correct point
	pt = [pt[0]+o[0], pt[1]+o[1], pt[2]+o[2]]
	
	# transform point
	if absolute:
		m = createMatrixFromObject(mesh)
		pt = trp(m, pt)
		
	return pt
# end getMeshCV()


# Gets the location of a point object
def getLocation(obj, center=False, pivot=False): 
	oObj = splitObjectName(obj)
	if oObj[2] == None:
		# object mode
		if center:
			cen = getAttr(oObj[1] + ".c")[0]
			parent = listRelatives(oObj[0], p=True, f=True)
			if parent:
				m = createMatrixFromObject(parent[0])
				cen = trp(m, cen)
			return cen
		else:
			m = createMatrixFromObject(oObj[1])
			if pivot:
				piv = getAttr(oObj[1] + ".sp")[0]
				return trp(m, piv)
			else:
				return trp(m, [0.0, 0.0, 0.0])
	elif oObj[2] == "vtx":
		# mesh cv
		return getMeshCV(oObj[1], oObj[3])	
	elif oObj[2] == "cv" or oObj[2] == "uv" or oObj[2] == "u" or oObj[2] == "ep" or oObj[2] == "smp" or oObj[2] == "pt" :
		# curve, surface or subd cv
		m = createMatrixFromObject(oObj[1])
		v = getAttr(oObj[0])[0]
		v = trp(m, v)
		return v
	else:
		# no location could be determined
		return None
# end getLocation()


# splits the name of an object. e.g:
# "subdivCube1.smp[2560][3]" => ["subdivCube1.smp[2560][3]", "subdivCube1", "smp", (2560,3)]
def splitObjectName(name):
	sName = name
	if sName.find("[") < 0:
		# object mode, no cv, etc.
		return  [sName, sName, None, None]
	
	sObj = sName[0:sName.find(".")]
	sType = sName[sName.find(".")+1:sName.find("[")]
	if sName.find("][") < 0:
		# one dimensional cv, etc.
		iCV = int(sName[sName.find("[")+1 : sName.find("]")])
		return  [sName, sObj, sType, iCV]
	else:
		sCv = sName[sName.find("[")+1 : sName.rfind("]")]
		iCv = tuple(map(int, sCv.split("][")))
		return  [sName, sObj, sType, iCv]
# end splitObjectName()


def wprint(text, title="Output", editable=False, width=420, height=160, onupdate=None, help=None):
	# Find valid window name
	winName  = title.replace(" ", "_")
	winName = "win_" + re.sub(r'\W+', '', winName).lower()
	
	def win_help(*args):
		wprint(help, title='Help for ' + title)
	
	def win_update(*args):
		newText = onupdate()
		scrollField(textField, edit=True, text=newText)
	
	def win_close(*args):
		deleteUI(winName)
		
	# Format text
	if type(text) == list or type(text) == tuple:
		text = "[" + join(map(str, text), ", ")  + "]"
	else:
		test = str(text)
	
	# Create window
	if window(winName, exists=True):
		deleteUI(winName)
	window(winName, title=title, widthHeight=(width, height))
	
	# Form layout
	oForm = formLayout(numberOfDivisions=100)
	
	# Button layout
	oButtons = formLayout(parent=oForm, numberOfDivisions=100)
	formLayout(oForm, edit=True, attachForm=[(oButtons, 'left', 2), (oButtons, 'bottom', 2), (oButtons, 'right', 2)])
	# Buttons
	if not help is None:
		btt1 = button(label=u"?", command=win_help)
		formLayout(oButtons, edit=True, attachPosition=[(btt1, 'left', 2, 0), (btt1, 'right', 2, 7)])
	if not onupdate is None:
		btt2 = button(label=u"Update", command=win_update)
		formLayout(oButtons, edit=True, attachPosition=[(btt2, 'left', 2, 38), (btt2, 'right', 2, 69)])
	btt3 = button(label=u"Close", command=win_close)
	formLayout(oButtons, edit=True, attachPosition=[(btt3, 'left', 2, 69), (btt3, 'right', 2, 100)])
	
	setParent('..')
	
	# Textfield
	textField = scrollField(wordWrap=True, editable=editable, text=text)
	formLayout(oForm, edit=True, attachForm=[(textField, 'left', 2), (textField, 'top', 2), (textField, 'right', 2)], attachControl=[(textField, 'bottom', 2, oButtons)])

	# Show window
	showWindow(winName)
# end wprint()


def userWin(title="Command", command=None, width=420, height=120, menuBar=True, help=None):
	# find suitable window name
	sTitle  = title.replace(" ", "_")
	sTitle = "win_" + re.sub(r'\W+', '', sTitle).lower()
	
	def win_ok(*args):
		command(args)
		deleteUI(sTitle)
	
	def win_apply(*args):
		command(args)
	
	def win_close(*args):
		deleteUI(sTitle)
	
	def win_help(*args):
		wprint(help, title='Help for ' + title)
	
	
	# create window
	if window(sTitle, exists=True):
		deleteUI(sTitle)
	oWin = window(sTitle, title=title, widthHeight=(width, height), menuBar=menuBar)
	
	# form layout
	oForm = formLayout(numberOfDivisions=100)
	
	# button layout
	oButtons = formLayout(parent=oForm, numberOfDivisions=100)
	formLayout(oForm, edit=True, attachForm=[(oButtons, 'left', 2), (oButtons, 'bottom', 2), (oButtons, 'right', 2)])
	# buttons
	if not help is None:
		oBtt1 = button(label=u"?", command=win_help)
	oBtt2 = button(label=u"Ok", command=win_ok)
	oBtt3 = button(label=u"Apply", command=win_apply)
	oBtt4 = button(label=u"Close", command=win_close)
	if not help is None:
		formLayout(oButtons, edit=True, attachPosition=[(oBtt1, 'left', 2, 0), (oBtt1, 'right', 2, 7), (oBtt2, 'left', 2, 7), (oBtt2, 'right', 2, 38), 
			(oBtt3, 'left', 2, 38), (oBtt3, 'right', 2, 69), (oBtt4, 'left', 2, 69), (oBtt4, 'right', 2, 100)])
	else:
		formLayout(oButtons, edit=True, attachPosition=[(oBtt2, 'left', 2, 0), (oBtt2, 'right', 2, 33), (oBtt3, 'left', 2, 33), (oBtt3, 'right', 2, 66), (oBtt4, 'left', 2, 66), (oBtt4, 'right', 2, 100)])
	
	# content layout
	oLayout = scrollLayout(horizontalScrollBarThickness=16, verticalScrollBarThickness=16, parent=oForm, height=1)
	formLayout(oForm, edit=True, attachForm=[(oLayout, 'left', 2), (oLayout, 'top', 2), (oLayout, 'right', 2)], attachControl=[(oLayout, 'bottom', 2, oButtons)])
	
	# return
	return oWin
# end userWin()


# ###############################################################
# Dependencies: meshTools.py
# ###############################################################

# Gets the position of all edges and their crease values
def getEdgeCreases(meshes=None):
	if not meshes:
		meshes = ls(sl=True)
	meshes = filterExpand(meshes, sm=12)
	if not meshes or len(meshes)==0:
		print "No mesh selected"
		return
	
	oCreases = []
	for oMesh in meshes:
		# get number of edges
		iEdgeCnt = polyEvaluate(oMesh, e=True)
		
		# get edge location and crease
		for i in range(iEdgeCnt):
			# edge
			oEdge =  oMesh + ".e[" + str(i) + "]"
			
			# get location
			sVtx = polyInfo(oEdge, ev=True)[0]
			sVtx = sVtx[sVtx.find(":")+1:]
			sVtx = sVtx.strip("\n ")
			sVtx = re.split(' +', sVtx)
			
			# get crease
			fCrease = max(0.0, polyCrease(oEdge, q=True, v=True)[0])
			
			# save edge location and crease
			v1 = getMeshCV(oMesh, int(sVtx[0]))
			v2 = getMeshCV(oMesh, int(sVtx[1]))
			oCreases.append([v1, v2, fCrease])
			
	return oCreases
# end 	getEdgeCreases()


def getVertexCreases(meshes=None):
	if not meshes:
		meshes = ls(sl=True)
	meshes = filterExpand(meshes, sm=12)
	if not meshes or len(meshes)==0:
		print "No mesh selected"
		return
	
	oCreases = []
	for oMesh in meshes:
		cnt = polyEvaluate(oMesh, v=True)
		
		for i in range(cnt):
			# vertex
			vtx =  oMesh + ".vtx[" + str(i) + "]"
			
			# get crease
			crease = max(0.0, polyCrease(vtx, q=True, vv=True)[0])
			
			# save edge location and crease
			p = getMeshCV(oMesh, i)
			oCreases.append([p, crease])
		
	return oCreases
# end 	getVertexCreases()


# ###############################################################
# End of dependencies
# ###############################################################


def subdInfo(subd=None,  ef=False, vf=False, fe=False, fv=False, ev=False, ve=False):
	# Save selection
	selection = ls(sl=True)
	
	if not subd:
		subd = filterExpand(selection, sm=68)
		if not subd or len(subd)==0:
			error("No mesh selected")
			return None
		subd = subd[0]
	
	# Get all components
	select(subd, r=True)
	if ef or ev:
		# convert selection to edges
		mel.eval("PolySelectConvert 2;")
		components = filterExpand(sm=37)
	elif vf or ve:
		# convert seletion to vertices
		mel.eval("PolySelectConvert 3;")
		components = filterExpand(sm=36)
	elif fe or fv:
		# convert selection to faces
		mel.eval("PolySelectConvert 1;")
		components = filterExpand(sm=38)
	else:
		print "No mode specified"
		return
	
	# get edges
	result = {}
	for vtx in components:
		select(vtx, r=True)
		if ef or vf:
			# convert selection to faces
			mel.eval("PolySelectConvert 1;")
			result[vtx] = filterExpand(sm=38)
		elif fe or ve:
			# convert selection to edges
			mel.eval("PolySelectConvert 2;")
			result[vtx] = filterExpand(sm=37)
		else:
			# convert seletion to vertices
			mel.eval("PolySelectConvert 3;")
			result[vtx] = filterExpand(sm=36)
		
	# restore selection
	select(selection, r=True)
	
	# return value
	return result
#end subdInfo()


def creaseSubdCorners(subd=None):
	if subd == None:
		subd = filterExpand(sm=68)
		if not subd or len(subd)==0:
			error("No mesh selected")
			return
		for obj in subd:
			creaseSubdCorners(obj)
		return
	
	# Save selection
	selection = ls(sl=True)
	
	# Get info
	info = subdInfo(subd, ve=True)
	
	for smp in info:
		if len(info[smp])==2:
			select(smp, r=True)
			mel.eval("FullCreaseSubdivSurface;")
		
	# restore selection
	select(selection, r=True)
#end creaseSubdCorners()


def polyToSubdiv2(mesh=None, threshold=2, maxPolyCount=10000, maxEdgesPerVert=50, tolerance=0.0001, mode=2):
	if mesh == None:
		mesh = filterExpand(sm=12)
		if not mesh or len(mesh)==0:
			error("No mesh selected")
			return
		subdivs = []
		for obj in mesh:
			subdivs.append(polyToSubdiv2(obj, threshold, maxPolyCount, maxEdgesPerVert, tolerance, mode))
		select(subdivs)
		return
	
	# TODO: Test automatic value for tolerance
	# TODO: Delete tolerance parameter as it should be automatic
	
	# calculate tolerance
	size = getAttr(mesh + ".bbsi")[0]
	size = math.sqrt(math.pow(size[0], 2) + math.pow(size[1], 2) + math.pow(size[2], 2))
	tolerance = size / 1e4
	
	# create subd
	subd = polyToSubdiv(mesh, ap=0, ch=0, aut=0, maxPolyCount=maxPolyCount, maxEdgesPerVert=maxEdgesPerVert)
	
	# check subd
	if not subd or len(subd)==0:
		return
	subd = subd[0]
	
	# get crease values for edges
	creaseValues = getEdgeCreases(mesh)
	
	# get information about edges
	edges = subdInfo(subd, ev=True)
	# crease edges
	for edge in edges:
		# find location of vertices
		p0 = getLocation(edges[edge][0])
		p1 = getLocation(edges[edge][1])
		
		# find out crease value
		creaseValue = -1
		for val in creaseValues:
			if (dist(val[0], p0) < tolerance and dist(val[1], p1) < tolerance) or (dist(val[0], p1) < tolerance and dist(val[1], p0) < tolerance):
				creaseValue = val[2]
				break
		
		# apply crease
		if creaseValue >= threshold:
			select(edge)
			mel.eval("FullCreaseSubdivSurface;")
		elif creaseValue > 0:
			select(edge)
			mel.eval("PartialCreaseSubdivSurface;")
	
	# get crease values for vertices
	creaseValues = getVertexCreases(mesh)
	
	# get information about vertices
	select(subd)
	mel.eval("PolySelectConvert 3;")
	vertices = filterExpand(sm=36)
	for vtx in vertices:
		# find location of vertex
		p = getLocation(vtx)
		
		# find out crease value
		creaseValue = -1
		for val in creaseValues:
			if dist(val[0], p) < tolerance:
				creaseValue = val[1]
				break
		
		# apply crease
		if creaseValue >= threshold:
			select(vtx)
			mel.eval("FullCreaseSubdivSurface;")
		elif creaseValue > 0:
			select(vtx)
			mel.eval("PartialCreaseSubdivSurface;")		
	
	# Crease corners
	creaseSubdCorners(subd)
	
	# Convert to nurbs
	if mode == 2:
		shape = listRelatives(subd, c=True, f=True)[0]
		subd = subdToNurbs(shape, ch=0, aut=1,  ot=0)[0]
		delete(shape)
	
	# restore selection
	select(subd, r=True)
	
	return subd
#end polyToSubdiv2()


def polyToSubdiv2Options():
	# get values
	threshold = GLOBALS.get("polyToSubdiv_threshold", 2)
	maxPolyCount = GLOBALS.get("polyToSubdiv_maxPolyCount", 10000)
	maxEdgesPerVert = GLOBALS.get("polyToSubdiv_maxEdgesPerVert", 32)
	tolerance = GLOBALS.get("polyToSubdiv_tolerance", 0.0001)
	mode = GLOBALS.get("polyToSubdiv_mode", 2)
	help = """PolyToSubdiv2 V1.4 - 29/09/2014

Written by Richard Wasenegger

Please take into account that this script will be rather
slow when used on very complex geometry.
"""
	
	def polyToSubdiv2_exec(*args):
		# get and save values
		threshold = floatSliderGrp("threshold",  q=True, v=True)
		maxPolyCount = intSliderGrp("maxPolyCount",  q=True, v=True)
		maxEdgesPerVert = intSliderGrp("maxEdgesPerVert",  q=True, v=True)
		#tolerance = floatSliderGrp("tolerance",  q=True, v=True)
		mode = radioButtonGrp("mode", q=True, sl=True)
		GLOBALS["polyToSubdiv_threshold"] = threshold
		GLOBALS["polyToSubdiv_maxPolyCount"] = maxPolyCount
		GLOBALS["polyToSubdiv_maxEdgesPerVert"] = maxEdgesPerVert
		GLOBALS["polyToSubdiv_tolerance"] = tolerance
		GLOBALS["polyToSubdiv_mode"] = mode
		
		# call command
		polyToSubdiv2(threshold=threshold, maxPolyCount=maxPolyCount, maxEdgesPerVert=maxEdgesPerVert, tolerance=tolerance, mode=mode)
		# set command history
		repeatLast(ac="python \"polyToSubdiv2(threshold=" + str(threshold) + ", maxPolyCount=" + str(maxPolyCount) + ", maxEdgesPerVert=" + str(maxEdgesPerVert) + ", tolerance=" + str(tolerance) + ", mode=" + str(mode) + ")\"")
	
	
	# create window
	oWin = userWin(u"Convert Polygons to Subdiv/NURBS", polyToSubdiv2_exec, width=410, height=135, help=help)
	
	# create controls
	intSliderGrp("maxPolyCount", label='Maximum base mesh faces:', field=True,  minValue=1, maxValue=50000, fieldMaxValue=100000, v=maxPolyCount)
	intSliderGrp("maxEdgesPerVert", label='Maximum edges per vertex:', field=True,  minValue=2, maxValue=50, fieldMaxValue=255, v=maxEdgesPerVert)
	floatSliderGrp("threshold", label='Threshold parially/full crease:', field=True, pre=4,  minValue=0, maxValue=10.0, v=threshold)
	#floatSliderGrp("tolerance", label='Tolerance:', field=True, pre=8,  minValue=0, maxValue=100.0, v=tolerance)
	radioButtonGrp("mode", numberOfRadioButtons=2, label='Convert to:', labelArray2=['Subdiv', 'NURBS'], sl=mode)
	
	# show window
	showWindow(oWin)
#end polyToSubdiv2Options()

def sc_PolyToNurbs_UI():

    polyToSubdiv2Options()
    myStats(29)