import maya.cmds as mc

def sc_Align_XY_UI():

    myIsTrue=mc.selectPref(q=True, trackSelectionOrder=True)

    if myIsTrue==1:

        selection=mc.ls(os=True)

        P1=mc.pointPosition(selection[-1])

        for i in range(len(selection)-1):
        
            mc.move(P1[0],P1[1],selection[i] ,xy=True,a=True)

    else:
        mc.error("Go Preferences > Selection --> Turn On TrackSelectionOrder")
        