#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 
#
#=================================================================================================
import maya.cmds as mc
import maya.OpenMaya as om
from functools import partial
from contourLib.myMeshContour import myContourTool
from contourLib.myCreateDynamicCutplane import myCreateDynamicCutplane
import shelf.youtube as yt

def play(*args):
    
    yt.play("Contouring")

def myToggle(*args):
    myMesh= mc.textField("tFld0", q=1 ,tx=1)
    myCutPlane= mc.textField("tFld1", q=1 ,tx=1)
    
    isOn=mc.getAttr("%s_Node_%s.deleteFaces"%(myCutPlane,myMesh))
    
    if isOn==1:
        mc.setAttr("%s_Node_%s.deleteFaces"%(myCutPlane,myMesh),0)
    
    else:
        mc.setAttr("%s_Node_%s.deleteFaces"%(myCutPlane,myMesh),1)
    

def myRun(*args):

    #Height = mc.floatField("my_H", q=1, v=1)
    myMesh= mc.textField("tFld0", q=1 ,tx=1)
    myCutPlane= mc.textField("tFld1", q=1 ,tx=1)
    
    q_RB1=mc.radioButton("RB1", query=True, select=True)
    q_RB2=mc.radioButton("RB2", query=True, select=True)
    crv_list=[]
    if q_RB1==True:
        
        stepHeight=mc.floatField("my_StepH",q=1,v=True)
        stepStart=mc.floatField("my_StepS",q=1,v=True)
        numberOfLevels=mc.intField("my_StepC",q=1,v=True)
        
        mc.move(0,0,stepStart,myCutPlane,a=True)
        
        crv_list=myContourTool(myMesh,myCutPlane,[stepHeight,numberOfLevels],"STEP" )
    
    
    if q_RB2==True:
        my_string=mc.textField("my_HList",q=1,tx=1)
        my_list =[float(i) for i in my_string.split(",") ]
        print my_list
        cutAtElevation=my_list
        crv_list=myContourTool(myMesh,myCutPlane,cutAtElevation,"AH" )
        
    #if checked create slab
    myBool= mc.checkBox("my_CB1",q=1, v=1)
    
    slabs=[]
    if myBool==1:
        sel=crv_list
    
        
        for i in range(len(sel)):
            pList=mc.getAttr('%s.cv[*]'%(sel[i]))
            s=mc.polyCreateFacet(p=pList,ch=False)
            mc.polyMergeVertex(s,d=0.001,am=True)
            slabs.extend(s)
            
    myOffset=mc.floatField("my_SlabOffset",q=1,v=1)
    
    if myOffset!=0.0:
    
        for i in range(len(slabs)):
                      
            mc.polyExtrudeFacet(slabs[i],off=myOffset)
            
            faceCount=mc.polyEvaluate(slabs[i],f=True)
            mc.delete('%s.f[1:%s]'%(str(slabs[i]),faceCount-1))
            
    mc.select(d=1)



def addFirstSel(TextFName,*args):
    sel = mc.ls(sl=True)
    add = mc.textField(TextFName, edit=True, text=sel[0])
    

def sc_ContourMesh_UI():  
    ##UI##########################################################################################START
     
    #Window Titel 
      
    win=mc.window(title="Contour Mesh | Create Slabs", iconName='Short Name', width=225 )

    col=mc.rowColumnLayout( numberOfColumns=2, columnAttach=(1, 'right', 0), columnWidth=[(1, 200), (2, 150)] )
    
    
    #radio button1
    
    mc.radioCollection()
    mc.text( label='' )
    mc.radioButton("RB1", label='Step Increment' ,sl=True)
    
    
    #Text Fields
    
    mc.text( label='Step Start Height' )
    mc.floatField("my_StepS",value=0.0)
    
    mc.text( label='Step Height' )
    mc.floatField("my_StepH",value=4.0)
    
    mc.text( label='Step Count (Number of Levels)' )
    mc.intField("my_StepC",value=20)
    
    mc.text( label='' )
    mc.text( label='' )
    
    mc.text( label='' )
    mc.radioButton("RB2", label='Absolute Height' )
    
    mc.text( label='Absolute Hight List' )
    mc.textField("my_HList",tx="0,4,8,12,16,22,26,30,30.5")
    
    mc.text( label='' )
    mc.text( label='' )
    
    
    #Check Box
    mc.text( label='' )
    CB1=mc.checkBox( "my_CB1",label='Create Slabs' )
    
    #Text Fields
    mc.text( label='Slab Offset' )
    mc.floatField("my_SlabOffset",value=0.5)

    mc.text( label='' )
    mc.text( label='' )

    #Add Button Textfield

    but=mc.button(l='Add Mesh to Contour', c=partial(addFirstSel,"tFld0"))
    textFld0=mc.textField('tFld0')

    but=mc.button(l='Add Cutplane Name   ', c=partial(addFirstSel,"tFld1"))
    textFld1=mc.textField('tFld1')
    
    #toggle Button

    mc.text( label='Toggle Cutplane Display' )
    mc.button( label='Toggle', command=myToggle)

    
    
    #Autofill if 2 things are selected surf+comp
    quickSel=mc.ls(sl=True,fl=True)
    if len(quickSel)==2:
        mc.textField(textFld0, edit=True, text=quickSel[0])
        mc.textField(textFld1, edit=True, text=quickSel[1])

    mc.text( label='' )
    mc.text( label='' )        
        
    #Run Button

    mc.text( label='' )
    mc.button( label='Run', command=myRun)
    
    #Help
    mc.text( label='' )
    mc.button( label='Help - Video', command=play )


    show=mc.showWindow()

    ##UI###########################################################################################END
