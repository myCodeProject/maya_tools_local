


import maya.cmds as mc
import maya.OpenMaya as om
import maya.OpenMayaMPx as ommpx

kPluginNodeName = "myMeshAreaNode"
#kcompUtilNodeClassify = "utility/general" 
kPluginNodeId = om.MTypeId(0x50001)

# Node definition
class myMeshAreaNode(ommpx.MPxNode):

    # input attributes
    inputAttr = om.MObject
    
    # output attributes
    outputAttr = om.MObject

    def __init__(self):
        ommpx.MPxNode.__init__(self)
        
    def compute(self, plug, dataBlock):
        
        # if these attributes are requested, recompute their values
        if (plug == myMeshAreaNode.outputAttr):
            
            # get the incoming data
            input_dataHandle = om.MDataHandle(dataBlock.inputValue(myMeshAreaNode.inputAttr))
            inMesh = input_dataHandle.asMesh()
            inMeshFn=om.MFnMesh(inMesh)
            
            #do somthing
            myResult=0.0
            polyIter = om.MItMeshPolygon(inMesh)
            while not polyIter.isDone():
                util=om.MScriptUtil()
                util.createFromDouble(0.0)
                ptr = util.asDoublePtr()
                polyIter.getArea(ptr,om.MSpace.kWorld)
                area=om.MScriptUtil(ptr).asDouble()
                myResult+=area
                
                polyIter.next()
            
            # set the output
            output_outputHandle = dataBlock.outputValue( myMeshAreaNode.outputAttr )
            output_outputHandle.setFloat( myResult )
            
            # set the plug clean so maya knows it can update
            dataBlock.setClean(plug)

# creator
def nodeCreator():
    return ommpx.asMPxPtr(myMeshAreaNode())

# initializer
def nodeInitializer():
    
    tAttr = om.MFnTypedAttribute()
    nAttr = om.MFnNumericAttribute()
    
    # input attributes
    myMeshAreaNode.inputAttr = tAttr.create("Input","In", om.MFnData.kMesh)
    
    # ouput attributes
    myMeshAreaNode.outputAttr = nAttr.create("Output","Out",om.MFnNumericData.kFloat, 0.0 )
    nAttr.setStorable(False)
    nAttr.setWritable(True)
    nAttr.setReadable(True)
    
    # add the attributes
    myMeshAreaNode.addAttribute(myMeshAreaNode.inputAttr)
    myMeshAreaNode.addAttribute(myMeshAreaNode.outputAttr)
    
    # Setup which attributes affect each other
    myMeshAreaNode.attributeAffects(myMeshAreaNode.inputAttr, myMeshAreaNode.outputAttr)

# initialize the script plug-in    
def initializePlugin(mobject):
    mplugin = ommpx.MFnPlugin(mobject, "Martin Pfleger", "1.0", "Any")
    try:
        mplugin.registerNode( kPluginNodeName, kPluginNodeId, nodeCreator, nodeInitializer )
    except:
        sys.stderr.write( "Failed to register command: %s\n" % kPluginNodeName )
        raise

# uninitialize the script plug-in
def uninitializePlugin(mobject):
    mplugin = ommpx.MFnPlugin(mobject)
    try:
        mplugin.deregisterNode( kPluginNodeId )
    except:
        sys.stderr.write( "Failed to unregister node: %s\n" % kPluginNodeName )
        raise
