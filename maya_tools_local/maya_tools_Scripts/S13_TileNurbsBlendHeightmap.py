#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 130417 
#
#=================================================================================================

#=================================================================================================
# Script:TileNurbsBlendHeightmap
# Dependencies : 
# Description : Tiles Nurbs surface with blend between polygon component based on color file with heightmap
# How to run : select polygon component1 + polygon component2 + nurbs surface
#
#=================================================================================================

#====================================================================================
#Script13
#TileNurbsBlendHeightmap(u,v,minHeight,maxHeight,index,surf,comp1,comp2,texture,HMtexture)

import maya.cmds as mc
from tileLib.tilePack import TileNurbsBlendHeightmap

#==================================================================================================
#USER INPUT:
u=1
v=30
minHeight=0.2
maxHeight=1.0
Texture='ramp1'
Heightmap='ramp1'
#==================================================================================================

selection=mc.ls(sl=True,fl=True)

#rebuild Nurbs parameter 0:1
theShape=mc.listRelatives(selection[2])
degreeU=mc.getAttr('%s.degreeU'%(theShape[0]))
degreeV=mc.getAttr('%s.degreeV'%(theShape[0]))
mc.rebuildSurface(selection[2],ch=True,rpo=1,rt=0,end=1,kr=0,kcp=1,kc=0,su=10,du=degreeU,sv=10,dv=degreeV,tol=0.0003,fr=0,dir=2)

#main command
TileNurbsBlendHeightmap(u,v,minHeight,maxHeight,'B',selection[2],selection[0],selection[1],Texture,Heightmap)

#====================================================================================
