
#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 150707
#
#=================================================================================================

#=================================================================================================
# Script:TileNurbsBlendOffset (Brick Arrangement) input XY direction
# Dependencies : 
# Description : Tiles Nurbs surface with blend between polygon component based on color file OFFSET
# How to run : select polygon component1 + polygon component2 + nurbs surface
#
#=================================================================================================

#====================================================================================
#ScriptJK
#TileNurbsBlendOffsetinputXY(u,v,ht,index,surf,comp1,comp2,texture,myX,myY)

import maya.cmds as mc
from tileLib.tilePack import TileNurbsBlendOffsetinputXY

#==================================================================================================
#USER INPUT:
u=20
v=50
direction=(1,0.5) #x,y direction vector for component
Height=1
Texture='ramp1'
#==================================================================================================

selection=mc.ls(sl=True,fl=True)

TileNurbsBlendOffsetinputXY(u,v,Height,'A',selection[2],selection[0],selection[1],Texture,direction[0],direction[1])

#====================================================================================
