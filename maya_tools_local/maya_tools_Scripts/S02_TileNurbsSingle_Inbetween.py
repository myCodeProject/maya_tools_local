#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 130417 
#
#=================================================================================================

#=================================================================================================
# Script:TileNurbsSingleInbetween
# Dependencies : 
# Description : Tiles single polygon component between 2 nurbs surfaces
# How to run : select polygon component + nurbs surface1 +nurbs surface2
#
#=================================================================================================

#====================================================================================
#Script2
#TileNurbsSingle(u,v,index,surf1,surf2,comp)

import maya.cmds as mc
from tileLib.tilePack import TileNurbsSingleInbetween

#==================================================================================================
#USER INPUT:
u=10
v=10
#==================================================================================================

selection=mc.ls(sl=True,fl=True)

#main command
TileNurbsSingleInbetween(u,v,'A',selection[1],selection[2],selection[0])

#====================================================================================