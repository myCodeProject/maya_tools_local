#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 130417 
#
#=================================================================================================

#=================================================================================================
# Script:TilePolyQuadBlend
# Dependencies : 
# Description : 
# How to run :
#
#=================================================================================================

#====================================================================================
#Script15
#TilePolyQuadBlendHeightmap(polyMaster1,polyMaster2,polyDestination,height)

import maya.cmds as mc
from tileLib.tilePack import TilePolyQuadBlendHeightmap

#==================================================================================================
#USER INPUT:
minHeight=0.25 #set value
maxHeight=1.0 #set value
#==================================================================================================

selection=mc.ls(sl=True,fl=True)
TilePolyQuadBlendHeightmap(selection[0],selection[1],selection[2],minHeight,maxHeight)

#====================================================================================
