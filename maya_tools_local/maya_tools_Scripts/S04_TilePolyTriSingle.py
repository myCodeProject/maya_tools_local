#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 130417 
#
#=================================================================================================

#=================================================================================================
# Script:TilePolyTriSingle
# Dependencies : 
# Description : tiles tri-polygon mesh with single tri-polygon component (if mesh has any faces that are not triangles script wont work)
# How to run : select tri-poly component + tri-poly mesh
#
#=================================================================================================

#====================================================================================
#Script4
#TilePolyTriSingle(polyMaster,polyDestination,height)

import maya.cmds as mc
from tileLib.tilePack import TilePolyTriSingle

#==================================================================================================
#USER INPUT:
Height=0.75 #set value
#==================================================================================================

selection=mc.ls(sl=True,fl=True)
TilePolyTriSingle(selection[0],selection[1],Height)

#====================================================================================