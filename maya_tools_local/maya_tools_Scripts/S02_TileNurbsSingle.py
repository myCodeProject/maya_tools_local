#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 130417 
#
#=================================================================================================

#=================================================================================================
# Script:TileNurbsSingle
# Dependencies : 
# Description : Tiles Nurbs surface with single polygon component
# How to run : select polygon component + nurbs surface
#
#=================================================================================================

#====================================================================================
#Script2
#TileNurbsSingle(u,v,ht,index,surf,comp)

import maya.cmds as mc
from tileLib.tilePack import TileNurbsSingle

#==================================================================================================
#USER INPUT:
u=2
v=2
Height=1
#==================================================================================================

selection=mc.ls(sl=True,fl=True)

#rebuild Nurbs parameter 0:1
#theShape=mc.listRelatives(selection[1])
#degreeU=mc.getAttr('%s.degreeU'%(theShape[0]))
#degreeV=mc.getAttr('%s.degreeV'%(theShape[0]))
#mc.rebuildSurface(selection[1],ch=True,rpo=1,rt=0,end=1,kr=0,kcp=1,kc=0,su=10,du=degreeU,sv=10,dv=degreeV,tol=0.0003,fr=0,dir=2)

#main command
TileNurbsSingle(u,v,Height,'A',selection[1],selection[0])

#====================================================================================