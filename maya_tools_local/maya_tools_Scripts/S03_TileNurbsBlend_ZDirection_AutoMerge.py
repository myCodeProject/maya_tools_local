#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 130417 
#
#=================================================================================================

#=================================================================================================
# Script:TileNurbsBlendDirectionZ
# Dependencies : 
# Description : Tiles Nurbs surface with blend between polygon component based on color file Z Direction Only+automerge
# How to run : select polygon component1 + polygon component2 + nurbs surface
#
#=================================================================================================

import maya.cmds as mc
from tileLib.tilePack import TileNurbsBlendDirectionZ
from tileLib.tilePack import TileNurbsBlend
from tileLib.tileMerger import autoMergeVtxPairs
from tileLib.tileMerger import getMergeListU
from tileLib.tileMerger import getMergeListV

#==================================================================================================
#USER INPUT:
u=10
v=10
Height=3
Texture='ramp1'
#==================================================================================================

selection=mc.ls(sl=True,fl=True)


name=TileNurbsBlendDirectionZ(u,v,Height,'A',selection[2],selection[0],selection[1],Texture)



meshName=name[0]
component=selection[0]

UList=getMergeListU(component)
VList=getMergeListV(component)

autoMergeVtxPairs(meshName,component,UList,VList,u,v)