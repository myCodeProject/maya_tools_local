#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 150724 
#
#=================================================================================================

#=================================================================================================
# Script:
# Dependencies : 
# Description : contours a mesh , generates resulting curves
# How to run : select clippingPlan + Mesh
#
#=================================================================================================

#====================================================================================
#Script28


import maya.cmds as mc
from contourLib.myMeshContour import myContourTool

#==================================================================================================
#USER INPUT:

createSlab='ON' #ON or OFF
slabOffsetValue=0.1
    
Mode=1 # 1 or 2

#Mode1
stepHeight=1
numberOfLevels=26

#OR

#Mode2
cutAtElevation=[1,2,3,4,5,6,7,8,9,10,15,20,25,30]
#==================================================================================================
sel2=mc.ls(sl=True,fl=True)
print sel2[1]
print sel2[0]

if Mode==1:
    
    a=myContourTool(sel2[1],sel2[0],[stepHeight,numberOfLevels],"STEP" )
    
if Mode==2:
    
    a=myContourTool(sel2[1],sel2[0],cutAtElevation,"AH")
    
print a    
#===============================================================================================
##Create Poly From Curve
if createSlab=='ON':

    sel=a
    
    slabs=[]
    for i in range(len(sel)):
        pList=mc.getAttr('%s.cv[*]'%(sel[i]))
        s=mc.polyCreateFacet(p=pList,ch=False)
        mc.polyMergeVertex(s,d=0.001,am=True)
        slabs.extend(s)
        
#===============================================================================================
##OffsetSlab

    if slabOffsetValue>0:
        print slabs
        for i in range(len(slabs)):
            
            
            mc.polyExtrudeFacet(slabs[i],off=slabOffsetValue)
            
            faceCount=mc.polyEvaluate(slabs[i],f=True)
            mc.delete('%s.f[1:%s]'%(str(slabs[i]),faceCount-1))
        
    mc.select(slabs)
    mc.select(sel,tgl=True)
#===============================================================================================     