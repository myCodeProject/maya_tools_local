#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 130417 
#
#=================================================================================================

#=================================================================================================
# Script:TileNurbsBlend
# Dependencies : 
# Description : Tiles Nurbs surface with blend between polygon component based on color file + AutoMerge
# How to run : select polygon component1 + polygon component2 + nurbs surface
#
#=================================================================================================

#====================================================================================
#Script3+AutoMerge
#TileNurbsBlend(u,v,ht,index,surf,comp1,comp2,texture)

import maya.cmds as mc
from tileLib.tilePack import TileNurbsBlend
from tileLib.tileMerger import autoMergeVtxPairs
from tileLib.tileMerger import getMergeListU
from tileLib.tileMerger import getMergeListV

#==================================================================================================
#USER INPUT:
u=10
v=10
Height=2
Texture='ramp1'
#==================================================================================================

selection=mc.ls(sl=True,fl=True)

name=TileNurbsBlend(u,v,Height,'A',selection[2],selection[0],selection[1],Texture)


meshName=name[0]
component=selection[0]

UList=getMergeListU(component)
VList=getMergeListV(component)

autoMergeVtxPairs(meshName,component,UList,VList,u,v)

#====================================================================================
