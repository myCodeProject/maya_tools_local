#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 130621 
#
#=================================================================================================

#=================================================================================================
# Script: QuadMesh To Diagrid
# Dependencies : 
# Description : reorders quadMesh* to diagridMesh
# How to run : select polygon run script
#
#=================================================================================================

#====================================================================================
#Script19

import maya.cmds as mc
from reorderMeshLib.myQuadMeshToDiagrid import myQuadMeshToDiagrid

selection=mc.ls(sl=True,fl=True)
polygonName=selection[0]
myQuadMeshToDiagrid(polygonName)

#====================================================================================
