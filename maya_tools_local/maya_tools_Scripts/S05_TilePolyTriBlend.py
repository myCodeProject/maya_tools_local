#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 130417 
#
#=================================================================================================

#=================================================================================================
# Script:TilePolyTriBlend
# Description : tiles tri-polygon mesh with blend between two tri-polygon components based on vertex color (if mesh has any faces that are not triangles script wont work)
# How to run : select tri-poly component1 + tri-poly component2 + tri-poly mesh
# How to run :
#
#=================================================================================================

#====================================================================================
#Script5
#TilePolyTriBlend(polyMaster1,polyMaster2,polyDestination,height)

import maya.cmds as mc
from tileLib.tilePack import TilePolyTriBlend

#==================================================================================================
#USER INPUT:
Height=0.75 #set value
#==================================================================================================

selection=mc.ls(sl=True,fl=True)
TilePolyTriBlend(selection[0],selection[1],selection[2],Height)

#====================================================================================

