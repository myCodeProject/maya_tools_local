#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 130417 
#
#=================================================================================================

#=================================================================================================
# Script:TilePolyQuadSingle
# Dependencies : 
# Description : 
# How to run :
#
#=================================================================================================

#====================================================================================
#Script14
#TilePolyQuadSingleHeightmap(polyMaster,polyDestination,height)

import maya.cmds as mc
from tileLib.tilePack import TilePolyQuadSingleHeightmap

#==================================================================================================
#USER INPUT:
minHeight=0.25 #set value
maxHeight=1.0 #set value
#==================================================================================================

selection=mc.ls(sl=True,fl=True)
TilePolyQuadSingleHeightmap(selection[0],selection[1],minHeight,maxHeight)

#====================================================================================
