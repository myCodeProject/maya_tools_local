#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 130417 
#
#=================================================================================================

#=================================================================================================
# Script:TileNurbsBlendInbetween
# Dependencies : 
# Description : Tiles Inbetween 2 Nurbs surfaces with blend between polygon component based on color file
# How to run : select polygon component1 + polygon component2 + nurbs surface1 + nurbs surface2
#
#=================================================================================================

#====================================================================================
#Script3
#TileNurbsBlend(u,v,index,surf1,surf2,comp1,comp2,texture)

import maya.cmds as mc
from tileLib.tilePack import TileNurbsBlendInbetween

#==================================================================================================
#USER INPUT:
u=30
v=30
Texture='ramp1'
#==================================================================================================

selection=mc.ls(sl=True,fl=True)

#main command
TileNurbsBlendInbetween(u,v,'A',selection[2],selection[3],selection[0],selection[1],Texture)

#====================================================================================
