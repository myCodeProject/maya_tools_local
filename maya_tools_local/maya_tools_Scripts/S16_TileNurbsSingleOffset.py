#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 130417 
#
#=================================================================================================

#=================================================================================================
# Script: TileNurbsSingleOffset
# Dependencies : 
# Description : Tiles Nurbs surface with single polygon component OFFSET
# How to run : select polygon component + nurbs surface
#
#=================================================================================================

#====================================================================================
#Script16
#TileNurbsSingleOffset(u,v,ht,index,surf,comp)

import maya.cmds as mc
from tileLib.tilePack import TileNurbsSingleOffset

#==================================================================================================
#USER INPUT:
u=20
v=20
Height=1
#==================================================================================================

selection=mc.ls(sl=True,fl=True)

#rebuild Nurbs parameter 0:1
theShape=mc.listRelatives(selection[1])
degreeU=mc.getAttr('%s.degreeU'%(theShape[0]))
degreeV=mc.getAttr('%s.degreeV'%(theShape[0]))
mc.rebuildSurface(selection[1],ch=True,rpo=1,rt=0,end=1,kr=0,kcp=1,kc=0,su=10,du=degreeU,sv=10,dv=degreeV,tol=0.0003,fr=0,dir=2)

#main command
TileNurbsSingleOffset(u,v,Height,'A',selection[1],selection[0])

#====================================================================================