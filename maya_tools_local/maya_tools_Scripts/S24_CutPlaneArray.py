#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 130621 
#
#=================================================================================================

#=================================================================================================
# Script: myCutplaneArray
# Dependencies : 
# Description : populates curve with an array of Nurbs planes
# How to run : select nurbs curve(s) and run script
#
#=================================================================================================

#=================================================================================================
#Script24

import maya.cmds as mc
from reorderNurbsLib.myCutplaneArray import myCutplaneArray

#==================================================================================================
#USER INPUT:
indexName="CutPlane"
cutPlaneCount=30
orientation="mode2"
#==================================================================================================


sel=mc.ls(sl=True,fl=True)

for c in range(len(sel)):
    myCutplaneArray(sel[c],indexName,cutPlaneCount,orientation)
    
#=================================================================================================
