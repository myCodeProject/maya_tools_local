#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 140525
#
#=================================================================================================

#=================================================================================================
# Script: suneExposure
# Dependencies :
# Description : generate sun exposure/face using vtx color 
# How to run : option 1 select polygon mesh (building) run script
#              option 2 select polygon mesh (building) + polygon mesh (obstructions) run script
#=================================================================================================

#====================================================================================
#Script11

import maya.cmds as mc
import maya.OpenMaya as om
from sunExposureLib.mySunExposure import mySunExposure

#example for sun direction list London,1.May.2014, 7:00-18:00, 1h interval
#sunDirectionList=[(-1.4142135623730949, 0.0, -1.0),(-1.3289260487773518, -0.48368952529595022, -1.0),(-1.0833504408394168, -0.90903895534408818, -1.0),(-0.70710678118654813, -1.2247448713915889, -1.0),(-0.24557560793794805, -1.3927284806400386, -1.0),(0.24557560793793431, -1.3927284806400411, -1.0),(0.70710678118654169, -1.2247448713915914, -1.0),(1.0833504408393724, -0.90903895534408774, -1.0),(1.3289260487772936, -0.48368952529595111, -1.0),(1.4142135623730949, 9.8607613152626476e-32, -1.0)]

#==================================================================================================

#USER INPUT:
sunDirectionList=[(-1,-1,-1)] #list of sun directions to average into 1 result (WARNING: to many sun directions and a too high polyfacecount will slow down the script considerably)
colorPallet=0 #color option 0=Black&White,1=Red&Yellow,2=Black&Yellow,3=Black&Red

#==================================================================================================

mySunExposure(sunDirectionList,colorPallet)


