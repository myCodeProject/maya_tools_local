#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 130417 
#
#=================================================================================================

#=================================================================================================
# Script: TessellateNurbs
# Dependencies : 
# Description : tessellates nurbs surface with tessellation Type (diaGrid,hexGrid,triGrid)
# How to run :
#
#=================================================================================================

#=================================================================================================
#Script1
#TessellateNurbs(u,v,nurbs,Type,texture)

import maya.cmds as mc
from tileLib.tilePack import TessellateNurbs

#==================================================================================================
#USER INPUT:
u=20
v=10
GridTyp='diaGrid' #regGrid,diaGrid,hexGrid,triGrid,regGridOffset,skewGrid
Texture='default' #optional, if no texture in scene leave default
#==================================================================================================

selection=mc.ls(sl=True,fl=True)

#rebuild Nurbs parameter 0:1
theShape=mc.listRelatives(selection[0])
degreeU=mc.getAttr('%s.degreeU'%(theShape[0]))
degreeV=mc.getAttr('%s.degreeV'%(theShape[0]))
mc.rebuildSurface(selection[0],ch=False,rpo=1,rt=0,end=1,kr=0,kcp=1,kc=0,su=10,du=degreeU,sv=10,dv=degreeV,tol=0.0003,fr=0,dir=2)

#main command
name=TessellateNurbs(u,v,selection[0],GridTyp,Texture)

#cleanup result
mc.polyMergeVertex(name[0],d=0.001,ch=False )
mc.polyAverageNormal(name[0],prenormalize=True,allowZeroNormal=0,postnormalize=0,distance=0.1 ,replaceNormalXYZ=[1, 0, 0])
mc.select(d=True)
mc.select(name)

#===================================================================================================