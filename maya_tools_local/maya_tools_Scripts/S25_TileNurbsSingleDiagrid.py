#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 150724 
#
#=================================================================================================

#=================================================================================================
# Script:
# Dependencies : 
# Description : Tiles Nurbs surface with single polygon component diagrid
# How to run : select polygon component + nurbs surface
#
#=================================================================================================

#====================================================================================
#Script25
#TileNurbsSingle(u,v,ht,index,surf,comp)

import maya.cmds as mc
from tileLib.tilePack import TileNurbsSingleDiagrid

#==================================================================================================
#USER INPUT:
u=8
v=10
Height=1
#==================================================================================================

selection=mc.ls(sl=True,fl=True)

#main command
TileNurbsSingleDiagrid(u,v,Height,'B',selection[1],selection[0])

#====================================================================================