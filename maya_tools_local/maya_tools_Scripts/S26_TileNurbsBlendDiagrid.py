#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 150724 
#
#=================================================================================================

#=================================================================================================
# Script:
# Dependencies : 
# Description : Tiles Nurbs surface with single polygon component diagrid
# How to run : select polygon component + nurbs surface
#
#=================================================================================================

#====================================================================================
#Script26
#TileNurbsSingle(u,v,ht,index,surf,comp1,comp2,texture)

import maya.cmds as mc
from tileLib.tilePack import TileNurbsBlendDiagrid

#==================================================================================================
#USER INPUT:
u=20
v=20
Height=1
texture="ramp1"
#==================================================================================================

selection=mc.ls(sl=True,fl=True)

#main command
TileNurbsBlendDiagrid(u,v,Height,'B',selection[0],selection[1],selection[2],texture)

#====================================================================================