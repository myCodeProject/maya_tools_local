#=================================================================================================
#
# Written by : Martin Pfleger
# Email : martin.pfleger@hotmail.co.uk
# Date : 160115 
#
#=================================================================================================

#=================================================================================================
# Script:
# Dependencies : 
# Description : select Quad Mesh then run script
# How to run : 
#
#=================================================================================================

import maya.cmds as mc
from myOffset.myOffsetByColor import myOffsetByColor

#select mesh
sel=mc.ls(sl=True, fl=True)
##create temp mesh with frozen tranforms
myD=mc.duplicate(sel[0])
mc.makeIdentity( myD[0],apply=True,t=1,r=1,s=1,n=0,pn=1)

###USER INPUT : #######

offsetByColor="OFF" # ON or OFF
offset=0.2

depthByColor="ON" # ON or OFF
depth=-0.5

#######################

#main script
myOffsetByColor(myD[0],offset,depth,offsetByColor,depthByColor)